﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 3.0f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float turnSpeed = 30.0f; // in degrees/second
	public float destroyRadius = 1.0f; 

	private bool movingForward = true;
	private BeeSpawner beeSpawner;


	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	
	// Update is called once per frame
	void Update() {


		// the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");

		// turn the car
		if (movingForward) {
			transform.Rotate (0, 0, -turn * (turnSpeed - (speed * 3)) * Time.deltaTime);
		} else {
			transform.Rotate (0, 0, turn * (turnSpeed - (speed * 3)) * Time.deltaTime);
		}
		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
			movingForward = true;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
			movingForward = false;
		} else {
			// braking
			if (speed > 0) {
				speed = Mathf.Max (speed - brake * Time.deltaTime, 0);
			} else if(speed < 0) {
				speed = Mathf.Min (speed + brake * Time.deltaTime, 0);
			}
		}



		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

	}

}
