﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;
	public float minBeePeriod, maxBeePeriod;
	private float timer;
	public float beePeriod;
	private int totalBees = 0;

	// Use this for initialization
	void Start () {
		beePeriod = Random.Range (minBeePeriod, maxBeePeriod);
		generateBees (nBees);
		timer = beePeriod;

	
	}

	
	// Update is called once per frame
	void Update () {
		if (beePeriod <= 0) {
			beePeriod = Random.Range (minBeePeriod, maxBeePeriod);
			generateBees (1);
		}
		beePeriod -= Time.deltaTime;
	}

	public void generateBees(int nBees) {
		// create bees
		for (int i = 0; i < nBees; i++) {
			// instantiate a bee
			BeeMove bee = Instantiate (beePrefab);

			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + totalBees;

			// move the bee to a random position within 
			// the bounding rectangle

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);

			totalBees++;
		}
}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// BUG! the line below doesn’t work
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}

}
