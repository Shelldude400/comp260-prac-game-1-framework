﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public Transform target1, target2;
	public Vector2 heading = Vector3.right; 
	public Transform target;
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	public ParticleSystem explosionPrefab;
	private float speed;
	private float turnSpeed;

	// Use this for initialization
	void Start () {
		// find a player object to be the target by type
		// Note: this is not standard Unity syntax
		PlayerMove player1 = FindObjectOfType<PlayerMove>();
		target1 = player1.transform;
		PlayerMove player2 = FindObjectOfType<PlayerMove>();
		target2 = player2.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
		
		 
	}
	
	// Update is called once per frame
	void Update () {

		// get the vector from the bee to the target 
		Vector2 temp = target1.position - transform.position;
		Vector2 temp2 = target2.position - transform.position;
		if (temp.magnitude < temp2.magnitude)
			target = target1;
		else
			target = target2;
			
		Vector2 direction = target.position - transform.position;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			// target on right, rotate clockwise
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}

}
